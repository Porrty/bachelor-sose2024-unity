using UnityEngine;

public class Inspector_Workstaiton : MonoBehaviour
{
    public GameObject ScrollView;
    public Transform Content;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            ScrollView.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            ScrollView.SetActive(false);
        }
    }

    public void AssignNewTransfromToUI(WorkStation workstation)
    {
        workstation.WorkstationUI.transform.SetParent(Content, false);
    }
}
