using UnityEngine;

public class Reward : MonoBehaviour
{
    public Item Item;
    public int Amount;
}
