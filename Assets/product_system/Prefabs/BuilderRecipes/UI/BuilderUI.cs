using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuilderUI : MonoBehaviour
{
    public List<BuildRecipe> BuildRecipes;

    public GameObject ScrollView;
    public GameObject Content;
    public GameObject MenuEntry;

    List<GameObject> MenuEntries = new();

    public Camera cam;

    public bool EditorMode = false;
    public GameObject ModelToCursor;

    [SerializeField] private LayerMask layermask;
    MenuEntryScript CurrentSelectedMenuEntry;
    WorkBasedTask buildTask;

    void Start()
    {
        foreach (var buildRecipe in BuildRecipes)
        {
            var menuEntrie = Instantiate(MenuEntry);
            menuEntrie.GetComponent<MenuEntryScript>().buildRecipe = buildRecipe;
            menuEntrie.GetComponentInChildren<Button>().onClick.AddListener(EnterEditorState);
            MenuEntries.Add(menuEntrie);

            menuEntrie.transform.SetParent(Content.transform, false);

            var displayUIText = "";
            displayUIText += "Name: " + buildRecipe.Name;
            displayUIText += "Needed Ingridients are: \n";
            foreach (var ingridient in buildRecipe.Recipe)
            {
                displayUIText += ingridient.Key.Name + ": " + Scheduler.CountOwnItems(ingridient.Key) + "/" + ingridient.Value + "\n";
            }
            menuEntrie.GetComponentInChildren<TextMeshProUGUI>().text = displayUIText;
        }
    }
    private void OnGUI()
    {
        UpdateUI();
    }

    void UpdateUI()
    {
        bool enoughItemsToCraft = false;
        foreach (var entry in MenuEntries)
        {
            var buildRecipe = entry.GetComponent<MenuEntryScript>().buildRecipe;

            var displayUIText = "";
            displayUIText += "Name: " + buildRecipe.Name;
            displayUIText += "Needed Ingridients are: \n";
            foreach (var ingridient in buildRecipe.Recipe)
            {
                var countedItems = Scheduler.CountOwnItems(ingridient.Key);
                displayUIText += ingridient.Key.Name + ": " + countedItems + "/" + ingridient.Value + "\n";

                if (countedItems >= ingridient.Value)
                {
                    enoughItemsToCraft = true;
                }
            }

            entry.GetComponent<MenuEntryScript>().canCraft = enoughItemsToCraft;

            displayUIText += "WorkType: " + buildRecipe.WorkType.ToString() + "\n";
            displayUIText += "WorkAmount: " + buildRecipe.WorkAmount.ToString() + "\n";
            entry.GetComponentInChildren<TextMeshProUGUI>().text = displayUIText;
            var buttonText = entry.GetComponentInChildren<Button>().GetComponentInChildren<TextMeshProUGUI>();
            if (enoughItemsToCraft == false)
            {
                buttonText.text = "<color=#ff0f0fff>CRAFT!</color>";
            }
            else
            {
                buttonText.text = "<color=#7cfc00ff>CRAFT!</color>";
            }
        }
    }

    void EnterEditorState()
    {
        Debug.Log("EditorModusStarted!");
        var currentSelectedButton = EventSystem.current.currentSelectedGameObject;
        var menuEntry = currentSelectedButton.GetComponentInParent<MenuEntryScript>();
        CurrentSelectedMenuEntry = menuEntry;
        if (menuEntry.canCraft == false)
        {
            return;
        }

        Scheduler.RemoveItemsFromSystem(menuEntry.buildRecipe.Recipe);
        ScrollView.SetActive(false);
        var buildModel = Instantiate(menuEntry.buildRecipe.Model);
        ModelToCursor = buildModel;
        buildModel.GetComponent<WorkStation>().enabled = false;
        EditorMode = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            ScrollView.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ScrollView.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (EditorMode == true)
        {
            // place work station to the world
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layermask.value))
            {
                ModelToCursor.transform.position = hit.point;
            }

            // place it down and start the workbased task
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("Leaving Editor Mode");
                EditorMode = false;
                var workParam = CurrentSelectedMenuEntry.buildRecipe;
                buildTask = new WorkBasedTask(
                    workParam.WorkType, 
                    workParam.WorkPriority, 
                    workParam.RequiredWorkLevel,
                    workParam.WorkAmount, 
                    workParam.CalorieConsumption, 
                    workParam.MentalConsumption,
                    hit.point);
                Scheduler.RegisterTask(buildTask);
            }
        }

        if (buildTask != null && buildTask.IsFinished())
        {
            ModelToCursor.GetComponent<WorkStation>().enabled = true;
            Scheduler.RemoveTask(buildTask);
            buildTask = null;
            CurrentSelectedMenuEntry = null;
        }
    }


}
