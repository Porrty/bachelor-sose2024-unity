using System;
using UnityEngine;

public static class PickAbleExtension
{
    public static bool IsPickable(this GameObject go)
    {
        return go.GetComponent<Reward>() != null;
    }
}

public class ItemConfig
{
    public MaterialType MatType;
    public float Weight;
    public float Nutrition;
    public float BuyValue;
    public float SellValue;
    public string Name;
    public int MaxStackCounter;
    public string Description;
}

[Serializable]
[CreateAssetMenu(menuName = "RecipeSystem/Create/Item")]
public class Item : ScriptableObject
{
    public Item Init(ItemConfig itemConfig)
    {
        MaterialType = itemConfig.MatType;
        Weigth = itemConfig.Weight;
        Nutrition = itemConfig.Nutrition;
        BuyValue = itemConfig.BuyValue;
        SellValue = itemConfig.SellValue;
        Name = itemConfig.Name;
        StackCounter = 1;
        MaxStackCounter = itemConfig.MaxStackCounter;
        Description = itemConfig.Description;
        return this;
    }

    /// <summary>
    /// TODO: Comments
    /// </summary>
    public MaterialType MaterialType;

    /// <summary>
    /// The weigth of the item.
    /// Interacts with the characters inventory weigth limit.
    /// </summary>
    [Tooltip("This item affects the weight in item containers.")]
    public float Weigth;


    /// <summary>
    /// The amount of kcal the worker gets if he eats items with nutrition.
    /// </summary>
    [SerializeField]
    public float Nutrition;

    /// <summary>
    /// The market buy value of the item.
    /// Interacts with the characters inventory gold amount.
    /// </summary>
    public float BuyValue;

    /// <summary>
    /// The sell value of the item.
    /// Interacts with the characters inventory gold amount.
    /// </summary>
    public float SellValue;

    /// <summary>
    /// The name of the item
    /// </summary>
    public string Name;

    /// <summary>
    /// Counts how many dubplicate items are stacked together in the inventory.
    /// Single items are left with 1.
    /// </summary>
    public int StackCounter;

    /// <summary>
    /// Maximum amount of that item, that can be stacked together on one unique item slot.
    /// </summary>
    public int MaxStackCounter;

    /// <summary>
    /// Used for the description of the item in a text box for the user.
    /// </summary>
    [SerializeField]
    [TextArea]
    public string Description;
}



