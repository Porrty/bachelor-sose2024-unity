using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "RecipeSystem/Create/BuildRecipe")]
public class BuildRecipe : ScriptableObject
{
    public GameObject Model;
    public ItemAmountDictionary Recipe;
    public string Name;
    public int WorkAmount;
    public WorkType WorkType;
    public WorkPriority WorkPriority = WorkPriority.HIGHEST;
    public int RequiredWorkLevel;
    public float CalorieConsumption;
    public float MentalConsumption;

    public void Init(
        GameObject Model,
        ItemAmountDictionary Recipe,
        string Name,
        int WorkAmount,
        WorkType WorkType,
        int RequiredWorkLevel,
        float CalorieConsumption,
        float MentalConsumption
        )
    {
        this.Model = Model;
        this.Recipe = Recipe;
        this.Name = Name;
        this.WorkAmount = WorkAmount;
        this.WorkType = WorkType;
        this.RequiredWorkLevel = RequiredWorkLevel;
        this.CalorieConsumption = CalorieConsumption;
        this.MentalConsumption = MentalConsumption;
    }

    public GameObject GetModel()
    {
        var child = Model.transform.Find("Model");
        return child.transform.GetChild(0).transform.gameObject;
    }
}