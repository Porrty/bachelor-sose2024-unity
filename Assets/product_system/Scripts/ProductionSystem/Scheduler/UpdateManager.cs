using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpdateable
{
    public void CustomUpdate(float dt);
}

public class UpdateManager
{
    private static UpdateManager _instance = null;

    private static List<IUpdateable> _updates = new();
    public static UpdateManager GetInstance()
    {
        if (_instance == null)
        {
            _instance = new();
        }
        return _instance;
    }

    public void Register(IUpdateable updateable)
    {
        // Debug.Log("IUpdateable has been registered: " + updateable);
        if (_updates.Contains(updateable) == false)
        {
            _updates.Add(updateable);
        }
    }

    public void Remove(IUpdateable updateable)
    {
        // Debug.Log("IUpdateable has been unregistered: " + updateable);
        if (_updates.Contains(updateable) == true)
        {
            _updates.Remove(updateable);
        }
    }

    public void Execute(float dt)
    {
        for (int i = 0; i < _updates.Count; i++)
        {
            _updates[i].CustomUpdate(dt);
        }
    }
}
