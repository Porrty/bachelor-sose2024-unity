using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class AdminCheat : MonoBehaviour
{
    ItemConfig icWood = new ItemConfig
    {
        MatType = MaterialType.WOOD,
        Weight = 1,
        Nutrition = 0,
        BuyValue = 0,
        SellValue = 0,
        Name = "Wood",
        MaxStackCounter = 999,
        Description = "A piece of wood!",
    };

    ItemConfig icStone = new ItemConfig
    {
        MatType = MaterialType.STONE,
        Weight = 1,
        Nutrition = 0,
        BuyValue = 0,
        SellValue = 0,
        Name = "Stone",
        MaxStackCounter = 999,
        Description = "A stone!",
    };

    ItemConfig icBerry = new ItemConfig
    {
        MatType = MaterialType.BERRY,
        Weight = 1,
        Nutrition = 10,
        BuyValue = 0,
        SellValue = 0,
        Name = "Berry",
        MaxStackCounter = 999,
        Description = "A berry!",
    };

    private void Start()
    {
        Scheduler.GetContainerOfMaterialClass(MaterialClass.CRAFTING_MATERIAL)
            .Add(ScriptableObject.CreateInstance<Item>().Init(icWood), 100);

        Scheduler.GetContainerOfMaterialClass(MaterialClass.CRAFTING_MATERIAL)
            .Add(ScriptableObject.CreateInstance<Item>().Init(icStone), 100);

        //  Scheduler.GetContainerOfMaterialClass(MaterialClass.NUTRITION)
        //      .Add(ScriptableObject.CreateInstance<Item>().Init(icBerry), 100);
    }

    private void Update()
    {
        // Num1 for 10x wood items
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            var container = Scheduler.GetContainerOfMaterialClass(MaterialClass.CRAFTING_MATERIAL);
            container.Add(ScriptableObject.CreateInstance<Item>().Init(icWood), 10);
        }

        // Num2 for 10x stone items
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            var container = Scheduler.GetContainerOfMaterialClass(MaterialClass.CRAFTING_MATERIAL);
            container.Add(ScriptableObject.CreateInstance<Item>().Init(icStone), 10);
        }

        // Num3 for 10x berry items
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            var container = Scheduler.GetContainerOfMaterialClass(MaterialClass.NUTRITION);
            container.Add(ScriptableObject.CreateInstance<Item>().Init(icBerry), 10);
        }

        // Num4 remove x10 wood 
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            var item = ScriptableObject.CreateInstance<Item>().Init(icWood);
            Scheduler.RemoveItemFromContainer(item, 10);
        }
    }
}
