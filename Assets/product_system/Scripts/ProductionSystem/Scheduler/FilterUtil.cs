using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Services.Analytics.Internal;
using UnityEngine;

public static class FilterUtil
{
    public static class WorkTask
    {
        public static List<IWorkTask> GetAllTasksOfType(List<IWorkTask> tasks, WorkType workType)
        {
            return new List<IWorkTask>(tasks.Where(task => task.GetWorkType() == workType));
        }
        public static List<IWorkTask> GetAllTasksOfPrio(List<IWorkTask> tasks, WorkPriority workPriority)
        {
            return new List<IWorkTask>(tasks.Where(task => task.GetWorkPriority() == workPriority));
        }

        public static IWorkTask GetNearestTasks(List<IWorkTask> tasks, Worker worker)
        {
            var curMinDistance = float.MaxValue;
            IWorkTask curTask = null;
            foreach (var task in tasks)
            {
                var distance = (task.GetDestination() - worker.transform.position).magnitude;
                if (task.IsWorkerSuitable(worker) == true &&
                    task.IsFinished() == false &&
                    distance < curMinDistance)
                {
                    curMinDistance = distance;
                    curTask = task;
                }
            }
            return curTask;

        }
    }
    
}
