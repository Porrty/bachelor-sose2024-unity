using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum EPlayerPos
{
    OUTSIDE = 0,
    INSIDE_STATUS_INFO = 1,
    INSIDE_RECIPE_SELECTION = 2
}

public class WorkBench : MonoBehaviour, IUpdateable, IWorkProcessUnit
{
    [Header("CONFIGURE")]
    public List<Recipe> recipeConf = new();
    public Transform FinalProductExitPos;

    [Header("GUI")]
    public GameObject RecipeSelectionGUI;
    public GameObject RecipeSelectionButton;
    public GameObject Queue;
    public GameObject Player;
    public EPlayerPos PlayerPos = EPlayerPos.OUTSIDE;
    public Transform content;
    public RecipeContainer recipeContainerPrefab;
    public List<RecipeContainer> recipeContainers = new();

    public IWorkTask workTask;
    public RecipeContainer activeContainerCraftingProcess;


    void Start()
    {
        Scheduler.RegisterWorkBench(this);

        foreach (var recipe in recipeConf)
        {
            var recContainer = Instantiate(recipeContainerPrefab);
            recipeContainers.Add(recContainer);
            recContainer.recipe = recipe;
            recContainer.workBench = this;
            recContainer.UpdateContainer();
            recContainer.transform.SetParent(content.transform, false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerPos++;
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerPos--;
    }

    private void Update()
    {
        switch (PlayerPos)
        {
            case EPlayerPos.OUTSIDE:
                Queue.SetActive(false);
                RecipeSelectionButton.SetActive(false);
                break;

            case EPlayerPos.INSIDE_STATUS_INFO:
                Queue.SetActive(true);
                RecipeSelectionButton.SetActive(false);
                break;

            case EPlayerPos.INSIDE_RECIPE_SELECTION:
                RecipeSelectionButton.SetActive(true);
                break;
        }

        if (RecipeSelectionButton.activeSelf == true
            && Input.GetKeyDown(KeyCode.F))
        {
            RecipeSelectionGUI.SetActive(true);
            Player.GetComponent<CameraMovement>().IsMovementAllowed = false;

            foreach (var container in recipeContainers)
            {
                container.UpdateContainer();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            RecipeSelectionGUI.SetActive(false);
            Player.GetComponent<CameraMovement>().IsMovementAllowed = true;
        }

        if (workTask == null)
        {
            Queue.GetComponent<TextMeshProUGUI>().text = "No items in the queue";
        }
        else
        {
            Queue.GetComponent<TextMeshProUGUI>().text = workTask.GetDebugInfo();
        }
    }

    public void CustomUpdate(float dt)
    {
        if (workTask != null && workTask.IsFinished() == true)
        {
            // task is finished
            Scheduler.RemoveTask(workTask);
            workTask = null;

            // drop final produkt into the game world
            if (activeContainerCraftingProcess != null)
            {
                var itemToDrop = activeContainerCraftingProcess.recipe.FinalProdukt;
                var itemObj = Instantiate(itemToDrop);
                itemObj.transform.position = FinalProductExitPos.position;
                Scheduler.RegisterTask(new TransportBasedTask(itemObj, 0.035f, 0.035f));
                activeContainerCraftingProcess = null;
            }
        }
    }
}
