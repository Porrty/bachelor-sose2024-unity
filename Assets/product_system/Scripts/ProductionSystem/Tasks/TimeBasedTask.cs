using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TimeBasedTask : IWorkTask, IUpdateable
{
    private WorkType _workType = WorkType.Waiting;
    private float _currentTime;
    private bool _isFinished = false;
    private float _neededTime;

    public TimeBasedTask(float neededTime)
    {
        _currentTime = neededTime;
        _neededTime = neededTime;
    }

    public void CustomUpdate(float dt)
    {
        if (_isFinished == true) return;

        if (_currentTime <= 0)
        {
            _isFinished = true;
        }

        _currentTime -= dt;
    }

    public void DoTask(Worker worker)
    {
        // do nothing only waiting
        return;
    }

    public void AssignWorker(Worker worker)
    {
        // Do nothing a time based task can not assign worker to work on a waiting process.
        return;
    }

    public void RemoveWorker(Worker worker)
    {
        // Do nothing a time based task can not assign worker to work on a waiting process.
        return;
    }

    public string GetCompletionInfo()
    {
        return _currentTime.ToString();
    }

    public bool IsFinished()
    {
        return _isFinished;
    }

    public bool IsWorkerSuitable(Worker worker)
    {
        // A time based task can not assign worker to work on a waiting process.
        return false;
    }

    public void ResetTask()
    {
        _isFinished = false;
        _currentTime = _neededTime;
    }

    public string GetDebugInfo()
    {
        string info = "";
        info += "TimeBasedTask: (" + WorkType.Waiting + ")\n";
        info += "Workers: none, can not assign for waiting\n";
        info += "Estimated Time: " + GetCompletionInfo() + "\n";
        return info;
    }

    public Vector3 GetDestination()
    {
        return Vector3.zero;
    }

    public void SetDestination(Vector3 workDestination)
    {
        // can not assign a task with the goal to wait with a destination
        return;
    }

    public WorkType GetWorkType()
    {
        return _workType;
    }

    public WorkClass GetWorkClass()
    {
        return WorkClass.Waiting;
    }

    public void DoTask(Worker worker, float dt)
    {
        // do nothing, gets never called
        return;
    }

    public float GetCalorieConsumption()
    {
        return 0;
    }

    public void AbortTask(Worker worker)
    {
        // do nothing - no worker gets assigned
        return;
    }

    public float GetMentalFatigue()
    {
        // no worker works on waiting
        return 0;
    }

    public WorkPriority GetWorkPriority()
    {
        return WorkPriority.LOW;
    }
}

