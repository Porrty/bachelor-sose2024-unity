using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class ItemGenerator : MonoBehaviour
{
    public static readonly string savePath = "Assets/product_system/Prefabs/Items/ScriptableObjects/";

    [MenuItem("Create/GenerateItems")]
    public static void GenItems()
    {
        var items = new List<Item>();

        var arrow = new ItemConfig()
        {
            MatType = MaterialType.ARROW,
            Weight = 8,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Arrow",
            MaxStackCounter = 1,
            Description = "A simple bow!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(arrow));

        var stone = new ItemConfig()
        {
            MatType = MaterialType.STONE,
            Weight = 3,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Stone",
            MaxStackCounter = 999,
            Description = "A basic example of a stone description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(stone));

        var wood = new ItemConfig()
        {
            MatType = MaterialType.WOOD,
            Weight = 3,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Wood",
            MaxStackCounter = 999,
            Description = "A basic example of a wood description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(wood));

        var berry = new ItemConfig()
        {
            MatType = MaterialType.BERRY,
            Weight = 0.2f,
            Nutrition = 15,
            BuyValue = 0,
            SellValue = 0,
            Name = "Berry",
            MaxStackCounter = 999,
            Description = "A basic example of a berry description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(berry));

        var paladium = new ItemConfig()
        {
            MatType = MaterialType.PALADIUM,
            Weight = 2,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Paladium",
            MaxStackCounter = 999,
            Description = "A basic example of a paladium description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(paladium));

        var palSphere = new ItemConfig()
        {
            MatType = MaterialType.PAL_SPHERE,
            Weight = 0.1f,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Palsphere",
            MaxStackCounter = 999,
            Description = "A basic example of a palsphere description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(palSphere));

        var meatCleaver = new ItemConfig()
        {
            MatType = MaterialType.MEAT_CLEAVER,
            Weight = 10f,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Meat Cleaber",
            MaxStackCounter = 999,
            Description = "A basic example of a meat cleaver description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(meatCleaver));

        var fiber = new ItemConfig()
        {
            MatType = MaterialType.FIBER,
            Weight = 0.5f,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Fiber",
            MaxStackCounter = 999,
            Description = "A basic example of a fiber description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(fiber));

        var grapplingHook = new ItemConfig()
        {
            MatType = MaterialType.GRAPPLING_HOOK,
            Weight = 1,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Grappling Hook",
            MaxStackCounter = 1,
            Description = "A basic example of a grappling hook description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(grapplingHook));

        var metalBar = new ItemConfig()
        {
            MatType = MaterialType.METAL_BAR,
            Weight = 5,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Metal Bar",
            MaxStackCounter = 1,
            Description = "A basic example of a metal bar description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(metalBar));

        var metalOre = new ItemConfig()
        {
            MatType = MaterialType.METAL_ORE,
            Weight = 8,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Metal Ore",
            MaxStackCounter = 1,
            Description = "A basic example of a metal ore description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(metalOre));

        var ancientComponent = new ItemConfig()
        {
            MatType = MaterialType.ANCIENT_COMPONENT,
            Weight = 5,
            Nutrition = 0,
            BuyValue = 0,
            SellValue = 0,
            Name = "Ancient Component",
            MaxStackCounter = 1,
            Description = "A basic example of a ancient component description!"
        };
        items.Add(ScriptableObject.CreateInstance<Item>().Init(ancientComponent));

        foreach (var item in items)
        {
            AssetDatabase.CreateAsset(item, savePath + item.Name + ".asset");
        }
        AssetDatabase.SaveAssets();
    }
}