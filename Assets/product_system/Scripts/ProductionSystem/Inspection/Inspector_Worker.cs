using TMPro;
using UnityEngine;

public class Inspector_Worker : MonoBehaviour
{
    public GameObject Content;
    public GameObject WorkerUI;
    public GameObject ScrollView;
    
    bool initialized = false;

    void Update()
    {
        if (initialized == false)
        {
            Scheduler.GetAllWorkers().ForEach(worker =>
            {
                worker.workerUI = Instantiate(WorkerUI);
                worker.workerUI.transform.SetParent(Content.transform, false);
            });
            initialized = true;
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            ScrollView.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            ScrollView.SetActive(false);
        }
    }
}