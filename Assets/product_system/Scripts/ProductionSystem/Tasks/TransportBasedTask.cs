using UnityEngine;
using System.Collections.Generic;

public enum TransportRuleSet
{
    WEIGHT,
    AMOUNT
}

public class TransportBasedTask : IWorkTask
{
    // tells the task how many items needed to be picked up
    Worker worker;
    public readonly float CalorieConsumption;
    public readonly float MentalConsumption;
    public bool _isFinished = false;
    public bool canBeAssigned = true;
    public Reward reward;

    public TransportBasedTask(Reward reward, float calorieConsumption, float mentalConsumption)
    {
        this.reward = reward;
        CalorieConsumption = calorieConsumption;
        MentalConsumption = mentalConsumption;
    }

    public void AssignWorker(Worker worker)
    {
        worker._currentAssignedTask = this;
        this.worker = worker;
    }

    public void DoTask(Worker worker, float dt)
    {
        if (reward.Amount <= 0)
        {
            reward.gameObject.SetActive(false);
            canBeAssigned = false;
        }

        if (worker.assignedContainer == null)
        {
            // pick up item
            worker.assignedContainer = null;
            worker.reward = Object.Instantiate(reward);

            // start new goal for the agent to get to the nearest item box
            worker.assignedContainer = Scheduler.
                GetNearestContainer(
                worker.transform.position,
                MaterialClassUtility.GetMaterialClass(reward.Item.MaterialType)
                );
            worker.Agent.SetDestination(worker.assignedContainer.GetPosition());

            // reduce the amount of the transportation task
            reward.Amount -= 2;
            if (reward.Amount >= 0)
            {
                worker.reward.Amount = 2;
            }
            else
            {
                // last item to pick up
                // will get be deleted when last item is placed into the container
                worker.reward.Amount = reward.Amount + 2;
                reward.gameObject.SetActive(false);
            }
        }
        else
        {
            // if pos is reached
            var watp = worker.Agent.transform.position;
            var wcg = worker.assignedContainer.GetPosition();

            Vector2 currentPos = new Vector2(watp.x, watp.z);
            Vector2 destinationPos = new Vector2(wcg.x, wcg.z);

            // if destination is reached
            if (Vector3.Distance(currentPos, destinationPos) < 10f)
            {
                // stop the worker
                worker.Agent.SetDestination(worker.transform.position);

                // unload the reward
                worker.assignedContainer.Add(worker.reward.Item, worker.reward.Amount);

                if (reward.Amount <= 0)
                {
                    _isFinished = true;
                }

                if (IsFinished() == true)
                {
                    Object.Destroy(reward.gameObject);
                    Scheduler.RemoveTask(this);
                    RemoveWorker(worker);
                }
                else
                {
                    worker.RemoveCurrentTask();
                }

                // remove the reward from the worker
                // destroy the copied version of the original reward
                Object.Destroy(worker.reward.gameObject);
                worker.reward = null;
                worker.assignedContainer = null;
            }
        }

    }

    public string GetDebugInfo()
    {
        return "Transportation Task";
    }

    public Vector3 GetDestination()
    {
        return new Vector3(reward.transform.position.x, 0, reward.transform.position.z);
    }

    public string GetCompletionInfo()
    {
        return "Status: " + IsFinished() + "\n";
    }

    public WorkType GetWorkType()
    {
        return WorkType.Transporting;
    }

    public bool IsFinished()
    {
        return _isFinished;
    }

    public bool IsWorkerSuitable(Worker worker)
    {
        if (canBeAssigned == false) return false;
        foreach (var ability in worker.WorkAbilities)
        {
            // dont check for work level
            // only the type is enough
            // level will increase the transportation amount that can be transported per run
            if (ability.WorkType == WorkType.Transporting)
            {
                return true;
            }
        }
        return false;
    }

    public void RemoveWorker(Worker worker)
    {
        worker.RemoveCurrentTask();
    }

    public void ResetTask()
    {
        // dont need to reset task, because the items lay on the ground and can be picked up
        return;
    }

    public void SetDestination(Vector3 workDestination)
    {
        // dont need, because the item in reward has the unity property of transform.position 
        return;
    }

    public WorkClass GetWorkClass()
    {
        return WorkClass.Utility;
    }

    public float GetCalorieConsumption()
    {
        return CalorieConsumption;
    }

    public void AbortTask(Worker worker)
    {
        // if worker holds an item
        if (worker.reward != null)
        {
            // worker drops the reward on the ground
            worker.reward.gameObject.transform.position = new Vector3(worker.transform.position.x, 0, worker.transform.position.z);

            // worker creates a new transport based task with the reward that was in the hand
            Scheduler.RegisterTask(new TransportBasedTask(worker.reward, GetCalorieConsumption(), GetMentalFatigue()));
        }

        // worker is now idle
        this.worker = null;
        worker.ChangeState(EState.IDLE);
        worker._currentAssignedTask = null;
        worker.reward = null;
        worker.assignedContainer = null;
    }

    public float GetMentalFatigue()
    {
        return MentalConsumption;
    }

    public WorkPriority GetWorkPriority()
    {
        return WorkPriority.LOW;
    }
}
