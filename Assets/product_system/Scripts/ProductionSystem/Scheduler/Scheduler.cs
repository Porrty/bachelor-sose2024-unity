using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class Scheduler : MonoBehaviour
{
    [SerializeField]
    private float _updateRate;
    [SerializeField]
    private float _targetDeltaTime;
    [SerializeField]
    private float _currentDeltaTime;

    private static List<IWorkTask> _task = new();
    private static List<IWorkTask> _low_task = new();
    private static List<IWorkTask> _mid_task = new();
    private static List<IWorkTask> _high_task = new();
    private static List<IWorkTask> _highest_task = new();

    private static HashSet<Worker> _workers = new();
    private static List<WorkStation> _stations = new();
    private static List<WorkBench> _workBenches = new();
    public static List<IContainer> _containers = new();
    public static List<Bed> _beds = new();

    public Reward rewardTest;

    public GameObject sun;
    private bool isDayOrNight = true;
    public bool IsDayOrNight
    {
        get { return isDayOrNight; }
        set
        {
            isDayOrNight = value;
            if (isDayOrNight == true)
            {
                AwakeWorkers();
            }
            else if (isDayOrNight == false)
            {
                InvokeWorkersGoToSleep();
            }
        }
    }
    private static float _currentWorldTime;
    private static float maxTimeCounterDayNight = ProductionSystemConfiguration.DayTimeDuration + ProductionSystemConfiguration.NightTimeDuration;

    public static GameObject worldUI;

    void Start()
    {
        _updateRate = ProductionSystemConfiguration.SchedulerTickRate;
        _targetDeltaTime = 1.0f / _updateRate;
        _currentDeltaTime = 0;
        _currentWorldTime = 0;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            RegisterTask(new TransportBasedTask(Instantiate(rewardTest), 0.003f, 0.003f));
        }

        // schedule Update manager with timed Executions
        if (_currentDeltaTime >= _targetDeltaTime)
        {
            // execute update manager
            UpdateManager.GetInstance().Execute(_currentDeltaTime);
            _currentDeltaTime = 0;
        }
        _currentDeltaTime += Time.deltaTime;

        // Update world timer
        if (_currentWorldTime < maxTimeCounterDayNight)
        {
            _currentWorldTime += (Time.deltaTime / 60); // increase the timer every minute
        }
        else
        {
            _currentWorldTime = 0;
        }

        // update sun pos
        sun.transform.rotation = Quaternion.AngleAxis(GetSunAngle(), new Vector3(1, 0, 0));

        // update day or night cycle + invoke worker go sleep and worker awake
        if (IsDayOrNight != IsDay())
        {
            IsDayOrNight = IsDay();
        }

        UpdateWorldInfo();
    }

    private void UpdateWorldInfo()
    {
        var debugTextWorld = "WorldInformation: \n";
        debugTextWorld += "Current WorldTimer: " + Scheduler.GetWorldTime() + "\n";
        debugTextWorld += "IsDay? : " + Scheduler.IsDay() + "\n";
        debugTextWorld += "Current Sun Angle: " + Scheduler.GetSunAngle() + "\n";
        worldUI.GetComponent<TextMeshProUGUI>().text = debugTextWorld;
    }

    public void InvokeWorkersGoToSleep()
    {
        foreach(var worker in _workers)
        {
            // let the worker sleep
            worker.ResetState();
            worker.InvokeSleep(GetNearestFreeBed(worker.transform.position));
        }
    }

    public void AwakeWorkers()
    {
        foreach (var worker in _workers)
        {
            worker.WakeUp();
        }
    }

    public static float GetWorldTime()
    {
        return _currentWorldTime;
    }

    public static float GetSunAngle()
    {
        float sunAngle;
        // split the sun in half
        // 0 - 180 is the range of the sun in daylight
        if (IsDay() == true)
        {
            sunAngle = (_currentWorldTime / ProductionSystemConfiguration.DayTimeDuration) * 180;
        }
        // 180 - 360 is the range of the moon
        else
        {
            sunAngle = 180 + ((_currentWorldTime - ProductionSystemConfiguration.DayTimeDuration)
                / ProductionSystemConfiguration.NightTimeDuration * 180);
        }
        return sunAngle;
    }

    public static bool IsDay()
    {
        return _currentWorldTime < ProductionSystemConfiguration.DayTimeDuration;
    }

    public static void RegisterTask(IWorkTask task)
    {
        if (task != null & _task.Contains(task) == false)
        {
            _task.Add(task);

            switch (task.GetWorkPriority())
            {
                case WorkPriority.LOW:
                    _low_task.Add(task);
                    break;

                case WorkPriority.MIDDLE:
                    Debug.Log("Added Mid task" + task.GetDebugInfo());
                    _mid_task.Add(task);
                    break;

                case WorkPriority.HIGH: 
                    _high_task.Add(task);
                    break;

                case WorkPriority.HIGHEST: 
                    _highest_task.Add(task);
                    break;
            }

            if (task.GetWorkType() == WorkType.Waiting)
            {
                IUpdateable itask = task as IUpdateable;
                UpdateManager.GetInstance().Register(itask);
            }
        };
    }

    public static void RemoveTask(IWorkTask task)
    {
        if (task != null & _task.Contains(task) == true)
        {
            _task.Remove(task);

            if (task.GetWorkType() == WorkType.Transporting)
            {
                Debug.Log("Transporting task removed");
            }

            switch (task.GetWorkPriority())
            {
                case WorkPriority.LOW:
                    _low_task.Remove(task);
                    break;

                case WorkPriority.MIDDLE:
                    _mid_task.Remove(task);
                    break;

                case WorkPriority.HIGH:
                    _high_task.Remove(task);
                    break;

                case WorkPriority.HIGHEST:
                    _highest_task.Remove(task);
                    break;
            }
        };
    }

    public static void RegisterContainer(IContainer container)
    {
        if (_containers.Contains(container) == false)
        {
            _containers.Add(container);
        };
    }

    public static void RemoveContainer(IContainer container)
    {
        if (_containers.Contains(container) == true)
        {
            _containers.Remove(container);
        };
    }

    public static IContainer GetNearestContainer(Vector3 startPos, MaterialClass materialClass)
    {
        float curDist = float.PositiveInfinity;
        IContainer curContainer = null;
        Vector2 startPos2d = new(startPos.x, startPos.z);
        foreach (var container in _containers)
        {
            if (container.IsMaterialClassCompatible(materialClass) == true)
            {
                Vector2 containerPos2d = new(container.GetPosition().x, container.GetPosition().z);
                var distance = Vector2.Distance(startPos2d, containerPos2d);
                if (distance < curDist)
                {
                    curDist = distance;
                    curContainer = container;
                }
            }
        }
        return curContainer;
    }

    public static IContainer GetContainerOfMaterialClass(MaterialClass mc)
    {
        float curDist = float.PositiveInfinity;
        IContainer curContainer = null;
        Vector2 startPos2d = new(0, 0);
        foreach (var container in _containers)
        {
            if (container.IsMaterialClassCompatible(mc) == true)
            {
                Vector2 containerPos2d = new(container.GetPosition().x, container.GetPosition().z);
                var distance = Vector2.Distance(startPos2d, containerPos2d);
                if (distance < curDist)
                {
                    curDist = distance;
                    curContainer = container;
                }
            }
        }
        return curContainer;
    }

    public static Bed GetNearestFreeBed(Vector3 startPos)
    {
        float curDist = float.PositiveInfinity;
        Bed curBed = null;
        Vector2 startPos2d = new(startPos.x, startPos.z);
        foreach (var bed in _beds)
        {
            Vector2 bedPos2d = new(bed.GetPosition().x, bed.GetPosition().z);
            var distance = Vector2.Distance(startPos2d, bedPos2d);
            if (distance < curDist && bed.IsAssigned() == false)
            {
                curDist = distance;
                curBed = bed;
            }
        }
        return curBed;
    }

    public static void RegisterWorker(Worker worker)
    {
        if (_workers.Contains(worker) == false)
        {
            _workers.Add(worker);
            UpdateManager.GetInstance().Register(worker);
        }
    }

    public static void RemoveWorker(Worker worker)
    {
        if (_workers.Contains(worker) == true)
        {
            _workers.Remove(worker);
            UpdateManager.GetInstance().Remove(worker);
        }
    }

    public static void RegisterWorkStation(WorkStation workStation)
    {
        if (_stations.Contains(workStation) == false)
        {
            _stations.Add(workStation);
            UpdateManager.GetInstance().Register(workStation);
        }
    }

    public static void RemoveWorkStation(WorkStation workStation)
    {
        if (_stations.Contains(workStation) == true)
        {
            _stations.Remove(workStation);
            UpdateManager.GetInstance().Remove(workStation);
        }
    }
    
    public static void RegisterWorkBench(WorkBench workBench)
    {
        if (_workBenches.Contains(workBench) == false)
        {
            _workBenches.Add(workBench);
            UpdateManager.GetInstance().Register(workBench);
        }
    }

    public static void RemoveWorkBench(WorkBench workBench)
    {
        if (_workBenches.Contains(workBench) == true)
        {
            _workBenches.Remove(workBench);
            UpdateManager.GetInstance().Remove(workBench);
        }
    }

    public static List<WorkBench> GetAllWorkBenches()
    {
        return new List<WorkBench>(_workBenches);
    }

    public static List<IWorkTask> GetAllTasks()
    {
        return new List<IWorkTask>(_task);
    }

    public static List<Worker> GetAllWorkers()
    {
        return _workers.ToList();
    }

    public static List<WorkStation> GetAllWorkStations()
    {
        return new List<WorkStation>(_stations);
    }

    public static void RegisterBed(Bed bed)
    {
        if (_beds.Contains(bed) == false) { _beds.Add(bed); }
    }

    public static void RemoveBed(Bed bed)
    {
        if (_beds.Contains(bed) == true) { _beds.Remove(bed); }
    }

    public static bool HasRecipeEnoughMaterials(Recipe recipe)
    {
        foreach( var ingridient in recipe.Ingridients)
        {
            if (CountOwnItems(ingridient.Key) < ingridient.Value)
            {
                return false;
            }   
        }
        return true;
    }

    public static int CountOwnItems(Item item)
    {
        int count = 0;
        foreach(var container in _containers)
        {
            count += container.Count(item);
        }
        return count;
    }

    public static void RemoveItemsFromSystem(Recipe recipe)
    {
        foreach(var ingridient in recipe.Ingridients)
        {
            Debug.Log("Want to delete " + ingridient.Key.Name + ": x" + ingridient.Value);
            RemoveItemFromContainer(ingridient.Key, ingridient.Value);
        }
    }

    public static void RemoveItemsFromSystem(ItemAmountDictionary iad)
    {
        foreach(var item in iad)
        {
            RemoveItemFromContainer(item.Key, item.Value);
        }
    }

    public static void RemoveItemFromContainer(Item item, int count)
    {
        var ownedItems = CountOwnItems(item);
        if (ownedItems < count)
        {
            throw new System.Exception("Can not remove more items than have!");
        }

        var deletedItems = 0;
        var curCount = count;
        foreach(var container in _containers)
        {
            if (curCount <= 0) return;
            deletedItems += container.Remove(item, curCount);
            curCount = count - deletedItems;
        }
    }

    public static List<IWorkTask> GetLowTasks()
    {
        return _low_task;
    }

    public static List<IWorkTask> GetMidTasks()
    {
        return _mid_task;
    } 

    public static List<IWorkTask> GetHighTasks()
    {
        return _high_task;
    }

    public static List<IWorkTask> GetHighestTasks()
    {
        return _highest_task;
    }
}
