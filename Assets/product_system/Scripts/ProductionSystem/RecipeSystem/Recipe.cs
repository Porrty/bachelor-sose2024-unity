using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "RecipeSystem/Create/Recipe")]
public class Recipe : ScriptableObject
{
    /// <summary>
    /// This dictionary lists the items required for production.
    /// It saves a pair of a Item object and the amount of this item needed.
    /// </summary>
    public ItemAmountDictionary Ingridients;
    public Reward FinalProdukt;
    public int Amount;
    
    public string Name;
    public int WorkAmount;
    public WorkType WorkType;
    public WorkPriority WorkPriority;
    public int RequiredWorkLevel;
    public float CalorieConsumption;
    public float MentalConsumption;

    public Recipe Init(
        string name,
        ItemAmountDictionary ingridients,
        int amount,
        int workAmount,
        WorkType workType,
        WorkPriority workPriority,
        int requiredWorkLevel,
        float calorieConsumption,
        float mentalConsumption
        )
    {
        Name = name;
        Ingridients = ingridients;
        Amount = amount;
        WorkAmount = workAmount;
        WorkType = workType;
        WorkPriority = workPriority;
        RequiredWorkLevel = requiredWorkLevel;
        CalorieConsumption = calorieConsumption;
        MentalConsumption = mentalConsumption;
        return this;
    }


}


