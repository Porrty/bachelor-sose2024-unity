using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Camera cam;

    public float vel = 5.0f;
    public float sens = 5.0f;

    public bool IsMovementAllowed = true;

    private void OnMouseDown()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (IsMovementAllowed == false)
        {
            return;
        }

        transform.position += transform.forward * Input.GetAxis("Vertical") * vel * Time.deltaTime;
        transform.position += transform.right * Input.GetAxis("Horizontal") * vel * Time.deltaTime;

        if (Input.GetKey(KeyCode.Space))
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");
            transform.eulerAngles += new Vector3(-mouseY * sens, mouseX * sens, 0);
        }
        
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit, 100))
        {
            // Transform objectHit = hit.transform;
            // Debug.Log(objectHit.name);
            // Debug.DrawRay(ray.origin, ray.direction, Color.red);
            Debug.DrawLine(ray.origin, ray.direction.normalized * 100, Color.red);
        }
    }
}
