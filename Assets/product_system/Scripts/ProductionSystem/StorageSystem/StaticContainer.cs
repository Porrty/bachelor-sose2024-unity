using System.Collections.Generic;
using TMPro;
using UnityEngine;

public interface IContainer
{
    bool Add(Item item, int amount);

    void Dispose();

    bool Remove(Item item);

    int Remove(Item item, int count);

    int Count(Item item);

    int Count(MaterialClass matClass);

    bool IsMaterialClassCompatible(MaterialClass materialClass);

    Item GetFood();

    Vector3 GetPosition();
}

public class StaticContainer : MonoBehaviour, IContainer
{
    [SerializeField]
    private Item[] items;

    [SerializeField]
    private int containerSize;

    public TextMeshPro DebugText;

    public List<MaterialClass> allowedMaterials = new();

    public bool IsMaterialClassCompatible(MaterialClass materialClass)
    {
        return allowedMaterials.Contains(materialClass);
    }

    public bool Add(Item item, int amount)
    {
        if (item == null) return false;

        int freeSlotIndex = -1;

        for (int i = 0; i< items.Length; i++)
        {
            // check if the item already exist in the container
            // and increase the counter
            if (items[i] != null && items[i].Name == item.Name)
            {
                items[i].StackCounter += amount;
                if (items[i].StackCounter > items[i].MaxStackCounter)
                {
                    items[i].StackCounter = items[i].MaxStackCounter;
                }
                return true;
            }
        }

        for (int i = 0; i < items.Length; i++)
        {
            // if item doesnt exist register it
            if (items[i] == null)
            {
                freeSlotIndex = i;
            }
        }

        if (freeSlotIndex == -1)
        {
            return false;
        }
        else
        {
            item.StackCounter = amount;
            items[freeSlotIndex] = item;
            return true;
        }
    }

    public void Dispose()
    {
        items = new Item[containerSize];
    }

    public bool Remove(Item item)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].Name == item.Name)
            {
                items[i] = null;
                return true;
            }
        }

        return false;
    }

    void Start()
    {
        items = new Item[containerSize];
        Scheduler.RegisterContainer(this);
    }

    void Update()
    {
        string debugText = "Items inside Container: \n";

        for (int i = 0; i < containerSize; i++)
        {
            if (items[i] != null)
            {
                debugText += "Name: " + items[i].Name + " \n";
                debugText += "Amount: " + items[i].StackCounter + " \n";
            }
        }

        DebugText.text = debugText;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public int Count(Item item)
    {
        int count = 0;
        foreach(var i in items)
        {
            if (i != null && i.Name == item.Name)
            {
                // Debug.Log("found item of type +" + item.Name + " and a amount of: " + i.StackCounter);
                count += i.StackCounter;
            }
        }
        return count;
    }

    public int Remove(Item item, int count)
    {
        for (var i = 0; i < items.Length; i++)
        {
            if (items[i] != null && items[i].Name == item.Name)
            {
                if (count > items[i].StackCounter)
                {
                    var removedItems = items[i].StackCounter;
                    items[i] = null;
                    return removedItems; 
                }
                else
                {
                    items[i].StackCounter -= count;
                    return count;
                }
            }
        }
        return -1;
    }

    public Item GetFood()
    {
        foreach(var item in items)
        {
            if (item == null) continue;

            if (MaterialClassUtility.GetMaterialClass(item.MaterialType) == MaterialClass.NUTRITION)
            {
                if (item.StackCounter > 1)
                {
                    item.StackCounter -= 1;
                }
                else if (item.StackCounter == 1)
                {
                    return item;
                }
                
                return item;
            }
        }
        return null;
    }

    public int Count(MaterialClass matClass)
    {
        int count = 0;
        foreach (var i in items)
        {
            if (i != null && MaterialClassUtility.GetMaterialClass(i.MaterialType) == matClass)
            {
                count += i.StackCounter;
            }
        }
        return count;
    }
}
