using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RecipeContainer : MonoBehaviour
{
    public Recipe recipe;
    public WorkBench workBench;

    public TextMeshProUGUI recipeDescription;
    public Button btn;

    private void Start()
    {
        btn.onClick.AddListener(DoCraft);
    }

    public void UpdateContainer()
    {
        recipeDescription.text = recipe.Name + "\n";
        recipeDescription.text += "WorkAmount: " + recipe.WorkAmount + "\n";
        recipeDescription.text += "WorkType: " + recipe.WorkType + "\n";
        recipeDescription.text += "Ingridients {\n";
        foreach (var i in recipe.Ingridients.Keys)
        {
            recipeDescription.text += "\t" + i.Name + " " + Scheduler.CountOwnItems(i) + "/" + recipe.Ingridients[i].ToString() + "\n";
        }
        recipeDescription.text += "}";
    }

    public void DoCraft()
    {
        Debug.Log("Clicked Craft Button");
        if (Scheduler.HasRecipeEnoughMaterials(recipe) == true)
        {
            Debug.Log("we have enough materials for the crafting process");
            workBench.workTask = new WorkBasedTask(recipe.WorkType, WorkPriority.HIGH, 1, 2, 0.035f, 0.035f, workBench.transform.position);
            workBench.activeContainerCraftingProcess = this;

            Scheduler.RemoveItemsFromSystem(recipe);
            Scheduler.RegisterTask(workBench.workTask);

            UpdateContainer();

            Debug.Log("Task was assigned into the scheduler");
        }
        
    }
}
