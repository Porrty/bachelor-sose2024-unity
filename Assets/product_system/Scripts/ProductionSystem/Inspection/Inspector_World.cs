using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class Inspector_World : MonoBehaviour
{
    public GameObject Content;
    public GameObject WorldUI;
    public GameObject ScrollView;

    void Start()
    {
        Scheduler.worldUI = Instantiate(WorldUI);
        Scheduler.worldUI.transform.SetParent(Content.transform, false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            ScrollView.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            ScrollView.SetActive(false);
        }
    }
}
