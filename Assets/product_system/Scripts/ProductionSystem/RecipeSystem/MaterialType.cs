public enum MaterialClass
{
    CRAFTING_MATERIAL,
    WEAPON,
    NUTRITION,
    ARMOR,
    AMMUNITION,
    BLUEPRINTS,
    UNIQUE_ITEM
}

public enum MaterialType
{
    WOOD,
    STONE,
    WOOL,
    CLOTH,
    ARROW,
    BERRY,
    PALADIUM,
    PAL_SPHERE,
    MEAT_CLEAVER,
    FIBER,
    GRAPPLING_HOOK,
    METAL_BAR,
    METAL_ORE,
    ANCIENT_COMPONENT
}
// erweitern !!!

public static class MaterialClassUtility
{
    public static MaterialClass GetMaterialClass(MaterialType materialType)
    {
        switch (materialType)
        {
            case MaterialType.WOOD:
            case MaterialType.STONE:
            case MaterialType.WOOL:
            case MaterialType.CLOTH:
            case MaterialType.PALADIUM:
            case MaterialType.FIBER:
            case MaterialType.METAL_ORE:
            case MaterialType.ANCIENT_COMPONENT:
                return MaterialClass.CRAFTING_MATERIAL;

            case MaterialType.ARROW:
                return MaterialClass.AMMUNITION;

            case MaterialType.BERRY:
                return MaterialClass.NUTRITION;

            case MaterialType.MEAT_CLEAVER:
            case MaterialType.GRAPPLING_HOOK:
                return MaterialClass.WEAPON;

            case MaterialType.PAL_SPHERE:
                return MaterialClass.UNIQUE_ITEM;

            default:
                throw new System.Exception(
                    "Didnt exhaust all possible material types: missing switch block for: "
                        + materialType.ToString());
        }
    }
}