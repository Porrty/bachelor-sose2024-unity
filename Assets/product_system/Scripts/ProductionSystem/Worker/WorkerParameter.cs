using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WorkAbility
{
    public WorkType WorkType;
    public int WorkLevel;

    public WorkAbility(WorkType workType, int workLevel)
    {
        WorkType = workType;
        WorkLevel = workLevel;
    }

    public override bool Equals(object obj)
    {
        return obj is WorkAbility wa && WorkType == wa.WorkType && WorkLevel == wa.WorkLevel;
    }

    public override int GetHashCode()
    {
        return WorkType.GetHashCode() ^ WorkLevel.GetHashCode();
    }
}

[Serializable]
[CreateAssetMenu(menuName = "ProductionSystem/Create/WorkerParameter")]
public class WorkerParameter : ScriptableObject
{
    public List<WorkAbility> Abilities = new();
    public float WorkPower;
    public float MaxKcal;
    public float MaxMentalCondition;
}
