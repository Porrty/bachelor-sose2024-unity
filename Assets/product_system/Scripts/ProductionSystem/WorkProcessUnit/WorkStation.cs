using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[Serializable]
public class WorkStation : MonoBehaviour, IUpdateable, IWorkProcessUnit
{
    [Header("[CONFIGURATION]")]
    [SerializeField]
    public List<TaskParameter> _taskParam = new();
    public Transform EntrancePoint;
    public Transform RewardOutputPosition;
    private List<IWorkTask> _workTasks = new();

    [Header("[REWARD]")]
    public Reward reward;
    public int rewardAmount;
    public float RewardTransportCalorieConsumption;
    public float RewardMentalConsumption;

    [Header("[DEBUG]")]
    public TextMeshPro DebugText;

    [Header("UI")]
    public GameObject WorkstationUI;

    private Inspector_Workstaiton inspect_workstation;
    private int _taskIndex = 0;
    public bool IsBuild = false;
    public WorkBasedTask buildTask;

    void Start()
    {
        if (IsBuild == true)
        {
            Init();
        } else
        {
            
            Init();
        }
        
    }

    private void Init()
    {
        Debug.Log("Register workstation: " + name);
        Scheduler.RegisterWorkStation(this);

        foreach (var param in _taskParam)
        {
            switch (param.TaskType)
            {
                case TaskType.WorkBased:
                    // Debug.Log("Generated Work Based Task");
                    _workTasks.Add(
                        new WorkBasedTask(
                            param.WorkType,
                            param.WorkPriority,
                            param.RequiredWorkLevel,
                            param.WorkAmount,
                            param.CalorieConsumption,
                            param.MentalConsumption,
                            EntrancePoint.position));
                    break;

                case TaskType.TimeBased:
                    // Debug.Log("Generated Timed Based Task");
                    _workTasks.Add(
                        new TimeBasedTask(
                            param.NeededTime));
                    break;
            }
        }
        reward.Amount = rewardAmount;
        _taskIndex = 0;
        _workTasks[_taskIndex].ResetTask();
        Scheduler.RegisterTask(_workTasks[_taskIndex]);
        WorkstationUI = Instantiate(WorkstationUI);
        inspect_workstation = FindAnyObjectByType<Inspector_Workstaiton>();
        inspect_workstation.AssignNewTransfromToUI(this);
    }

    private void OnDestroy()
    {
        foreach (var task in _workTasks)
        {
            if (task != null)
            {
                Scheduler.RemoveTask(task);
            }
        }
        UpdateManager.GetInstance().Remove(this);
    }

    public void CustomUpdate(float dt)
    {
        if (_workTasks[_taskIndex].IsFinished() == true)
        {
            // if last task is finished
            if (_taskIndex == _workTasks.Count - 1) // starting from 0
            {
                // reset and remove last task in the list
                var lastTask = _workTasks[_taskIndex];
                lastTask.ResetTask();
                Scheduler.RemoveTask(lastTask);

                // go back at the beginning and schedule the first task back
                _taskIndex = 0;
                var firstTask = _workTasks[_taskIndex];
                Scheduler.RegisterTask(firstTask);

                // generate the reward
                // just throw it on the ground as a new TransportBasedTask
                var rewardObj = Instantiate(reward);
                reward.transform.position = RewardOutputPosition.position;
                Scheduler.RegisterTask(new TransportBasedTask(rewardObj, RewardTransportCalorieConsumption, RewardMentalConsumption));
                return;
            }

            // reset and remove the current task
            var currentTask = _workTasks[_taskIndex];
            currentTask.ResetTask();
            Scheduler.RemoveTask(currentTask);

            // Schedule the next task
            _taskIndex += 1;
            var nextTask = _workTasks[_taskIndex];
            Scheduler.RegisterTask(nextTask);
        }

        WorkstationUI.GetComponent<TextMeshProUGUI>().text = GetDebugInfo();
    }

    public string GetDebugInfo()
    {
        string debugText = "WorkStation: " + name + "\n";
        debugText += _workTasks[_taskIndex].GetDebugInfo();
        DebugText.text = debugText;
        return debugText;
    }
}
