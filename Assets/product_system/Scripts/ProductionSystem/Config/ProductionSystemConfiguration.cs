using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ProductionSystemConfiguration
{
    /// <summary>
    /// 
    /// </summary>
    public static float SchedulerTickRate = 15.0f;
    /// <summary>
    /// Default kcal consumption if a worker does no work.
    /// </summary>
    public static float WorkerIdleKcalConsumption = 0.1f;

    /// <summary>
    /// 1 Mental Point per 10s
    /// </summary>
    public static float WorkerMentalDefaultDecrease = 1.0f / 10.0f;

    /// <summary>
    /// Restores 1 Mental Point per 20s if a worker is taking a break
    /// </summary>
    public static float WorkerMentalDefaultBreakRegeneration = 1.0f / 20.0f;

    /// <summary>
    /// The rate at which the worker recovers his mental health when he sleeps.
    /// </summary>
    public static float WorkerMentalSleepRegeneration = 1.0f / 10.0f;

    /// <summary>
    /// The Duration of the day cycle in minutes.
    /// </summary>
    public static float DayTimeDuration = 1f;

    /// <summary>
    /// The duration of the night cycle in minutes.
    /// </summary>
    public static float NightTimeDuration = 1f;

    /// <summary> 
    /// 
    /// </summary>
    public static float minDistanceDetection = 10f;

    public static float workerIdleStateMovingIntervall = 5f;

    public static float normalWalkSpeed = 100f;

    public static float idleWalkSpeed = 1f;

    public static Dictionary<int, int> LevelIncrease = new()
    {
        { 1, 1 },
        { 2, 2 },
        { 3, 3 },
        { 4, 4 },
    };
}
