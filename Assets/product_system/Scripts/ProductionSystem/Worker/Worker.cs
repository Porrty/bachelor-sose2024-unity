using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public interface IState
{
    public void OnEnter(Worker worker);
    public void OnUpdate(Worker worker, float dt);
    public void OnExit(Worker worker);
}

public enum EState
{
    // dead state,
    DEAD,
    // idle state
    IDLE,
    // working
    WALKING_TO_TASK,
    WORKING,
    // sleeping
    WALKING_TO_BED,
    SLEEPING,
    // eating
    WALKING_TO_FOOD_SOURCE,
    EATING,
    // resting
    WALKING_TO_MENTAL_RESTORE_PLACE,
    RESTING
}

public class DeadState : IState
{
    public void OnEnter(Worker worker)
    {
        // dead do nothing
        return;
    }

    public void OnExit(Worker worker)
    {
        // dead do nothing
        return;
    }

    public void OnUpdate(Worker worker, float dt)
    {
        // dead do nothing
        return;
    }
}

public class IdleState : IState
{
    private readonly float _idleTimer = ProductionSystemConfiguration.workerIdleStateMovingIntervall;
    private float _currentIdleTimer;
    public Vector3 idlePos = new();

    public void OnEnter(Worker worker)
    {
        // Debug.Log("IDLE STATE ENTER:");
        _currentIdleTimer = _idleTimer;
        idlePos = GetRandomPosition(worker, -20, 20);
        // Debug.Log("POS: " + idlePos);
        worker.Agent.SetDestination(idlePos);
        // Debug.Log(worker.Agent.SetDestination(idlePos));
        worker.Agent.speed = ProductionSystemConfiguration.idleWalkSpeed;
    }

    public void OnExit(Worker worker)
    {
        if (worker.WorkAbilities.Contains(new WorkAbility(WorkType.Transporting, 1)))
        {
            worker.Agent.speed = 100;
        }
        else
        {
            worker.Agent.speed = ProductionSystemConfiguration.normalWalkSpeed;
        }

        _currentIdleTimer = _idleTimer;
    }

    public void OnUpdate(Worker worker, float dt)
    {
        // walk randomly
        _currentIdleTimer -= dt;
        if (_currentIdleTimer <= 0)
        {
            var watp = worker.Agent.transform.position;
            if (Vector3.Distance(watp, idlePos) < ProductionSystemConfiguration.minDistanceDetection)
            {
                idlePos = GetRandomPosition(worker, -5, 5);
                worker.Agent.SetDestination(idlePos);
            }
            _currentIdleTimer = _idleTimer;
        }

        // consume kcal
        worker.CurrentKcal -= ProductionSystemConfiguration.WorkerIdleKcalConsumption * dt;

        // regenerate mental health
        if (worker.CurrentMentalCondition < worker.MaxMentalCondition)
        {
            worker.CurrentMentalCondition += ProductionSystemConfiguration.WorkerMentalDefaultBreakRegeneration * dt;
        }
    }

    private Vector3 GetRandomPosition(Worker worker, float minValue, float maxValue)
    {
        // limit position of possible distance
        var currentPos = worker.Agent.transform.position;
        Vector3 randomPos = new Vector3(
            UnityEngine.Random.Range(minValue, maxValue),
            0,
            UnityEngine.Random.Range(minValue, maxValue));
        currentPos += randomPos;
        return currentPos;
    }
}

public class WalkToTaskState : IState
{
    public void OnEnter(Worker worker)
    {
        var targetPos = worker._currentAssignedTask.GetDestination();
        worker.Agent.SetDestination(new Vector3(targetPos.x, 0, targetPos.z));
    }

    public void OnExit(Worker worker)
    {
        worker.Agent.SetDestination(worker.Agent.transform.position);
    }

    public void OnUpdate(Worker worker, float dt)
    {
        var watp = worker.Agent.transform.position;
        var wcg = worker._currentAssignedTask.GetDestination();

        // if destination is reached
        if (Vector3.Distance(watp, wcg) <
            ProductionSystemConfiguration.minDistanceDetection)
        {
            worker.Agent.SetDestination(worker.Agent.transform.position);
            worker.ChangeState(EState.WORKING);
        }

        // consume kcal
        worker.CurrentKcal -= ProductionSystemConfiguration.WorkerIdleKcalConsumption * dt;

        // regenerate mental health
        if (worker.CurrentMentalCondition < worker.MaxMentalCondition)
        {
            worker.CurrentMentalCondition += ProductionSystemConfiguration.WorkerMentalDefaultBreakRegeneration * dt;
        }
    }
}

public class WorkingState : IState
{
    public void OnEnter(Worker worker)
    {
        return;
    }

    public void OnExit(Worker worker)
    {
        return;
    }

    public void OnUpdate(Worker worker, float dt)
    {
        // consume normal hunger
        worker.CurrentKcal -= worker._currentAssignedTask.GetCalorieConsumption() * dt;

        // decrease mental health
        if (worker.CurrentMentalCondition > 0)
        {
            worker.CurrentMentalCondition -= worker._currentAssignedTask.GetMentalFatigue() * dt;
        }

    }
}

public class WalkToBedState : IState
{
    public static float minDistance = 5f;
    public void OnEnter(Worker worker)
    {
        worker.Agent.SetDestination(worker.bed.GetPosition());
    }

    public void OnExit(Worker worker)
    {
        worker.Agent.SetDestination(worker.Agent.transform.position);
    }

    public void OnUpdate(Worker worker, float dt)
    {
        // check if destination is reached
        var watp = worker.Agent.transform.position;
        var wb = worker.bed.GetPosition();

        Vector2 currentPos = new Vector2(watp.x, watp.z);
        Vector2 destinationPos = new Vector2(wb.x, wb.z);

        // if destination is reached
        if (Vector3.Distance(currentPos, destinationPos) < minDistance)
        {
            worker.Agent.SetDestination(worker.Agent.transform.position);
            worker.ChangeState(EState.SLEEPING);
        }

        // consume kcal
        worker.CurrentKcal -= ProductionSystemConfiguration.WorkerIdleKcalConsumption * dt;
    }
}

public class SleepingState : IState
{
    public void OnEnter(Worker worker)
    {
        // do nothing
    }

    public void OnExit(Worker worker)
    {
        // remove assigned bed
        worker.bed = null;
    }

    public void OnUpdate(Worker worker, float dt)
    {
        // regenerate GEI when the worker is asleep
        if (worker.CurrentMentalCondition < worker.MaxMentalCondition)
        {
            worker.CurrentMentalCondition += ProductionSystemConfiguration.WorkerMentalSleepRegeneration * dt;
        }
        else
        {
            worker.CurrentMentalCondition = worker.MaxMentalCondition;
        }

    }
}

public class WalkingToFoodSourceState : IState
{
    private Vector3 targetPos = Vector3.zero;
    public void OnEnter(Worker worker)
    {
        // go to the nearest food source
        worker.assignedContainer = Scheduler.GetNearestContainer(worker.transform.position, MaterialClass.NUTRITION);
        targetPos = worker.assignedContainer.GetPosition();
        targetPos = new Vector3(targetPos.x, 0, targetPos.z);
        worker.Agent.SetDestination(targetPos);
    }

    public void OnExit(Worker worker)
    {

    }

    public void OnUpdate(Worker worker, float dt)
    {
        // check if destination is reached
        var curPos = worker.Agent.transform.position;
        curPos = new Vector3(curPos.x, 0, curPos.z);

        if (Vector3.Distance(curPos, targetPos) < ProductionSystemConfiguration.minDistanceDetection)
        {
            worker.Agent.SetDestination(worker.Agent.transform.position);
            worker.ChangeState(EState.EATING);
        }

        // consume kcal
        worker.CurrentKcal -= ProductionSystemConfiguration.WorkerIdleKcalConsumption * dt;
    }
}

public class EatingState : IState
{
    public void OnEnter(Worker worker)
    {
        Eat(worker);
    }

    public void OnExit(Worker worker)
    {
        worker.assignedContainer = null;
    }

    public void OnUpdate(Worker worker, float dt)
    {
        if (worker.CurrentKcal < (worker.MaxKcal / 2))
        {
            Eat(worker);
        }
        else
        {
            worker.ChangeState(EState.IDLE);
        }
    }

    private void Eat(Worker worker)
    {
        var foodToEat = worker.assignedContainer.GetFood();
        if (foodToEat != null)
        {
            worker.CurrentKcal += foodToEat.Nutrition;
            if (worker.CurrentKcal > worker.MaxKcal)
            {
                worker.CurrentKcal = worker.MaxKcal;
            }
        }
        else
        {
            worker.ChangeState(EState.IDLE);
        }
    }
}

public class WalkingToRestStationState : IState
{
    public void OnEnter(Worker worker)
    {
        throw new NotImplementedException();
    }

    public void OnExit(Worker worker)
    {
        throw new NotImplementedException();
    }

    public void OnUpdate(Worker worker, float dt)
    {
        throw new NotImplementedException();
    }
}


public class Worker : MonoBehaviour, IUpdateable
{
    // handeling pathfinding for traveling - prototype
    public NavMeshAgent Agent;
    // config parameter of the worker
    public WorkerParameter workParameter;

    public TextMeshPro debugText;

    public HashSet<WorkAbility> WorkAbilities = new();
    [field: System.NonSerialized]
    public float WorkPower;
    [field: System.NonSerialized]
    public float MaxKcal;
    [field: System.NonSerialized]
    public float CurrentKcal;
    [field: System.NonSerialized]
    public float MaxMentalCondition;
    [field: System.NonSerialized]
    public float CurrentMentalCondition;

    public IWorkTask _currentAssignedTask;
    private DeadState DEAD_STATE = new();
    private IdleState IDLE_STATE = new();

    private WalkToTaskState WALKING_TO_TASK_STATE = new();
    private WorkingState WORKING_STATE = new();

    private WalkToBedState WALKING_TO_BED_STATE = new();
    private SleepingState SLEEPING_STATE = new();

    private WalkingToFoodSourceState WALKING_TO_FOOD_SOURCE = new();
    private EatingState EATING_STATE = new();

    private WalkingToRestStationState WALKING_TO_REST_STATION_STATE = new();

    public IState _currentState;
    public EState CurrentEnumState;

    public GameObject itemPickUpPosition;
    public Reward reward;
    public IContainer assignedContainer;

    public Bed bed;

    public GameObject workerUI;

    void Start()
    {
        foreach (var wpa in workParameter.Abilities)
        {

            var success = WorkAbilities.Add(new WorkAbility(wpa.WorkType, wpa.WorkLevel));
            if (success == false)
            {
                Debug.Log(
                    "\"<color=red>Error: Couldnt add workability " +
                    wpa.WorkType +
                    " from work parameter. Probabily a duplicate?</color>" + "from" + this.name);
            }
        }
        WorkPower = workParameter.WorkPower;
        MaxKcal = workParameter.MaxKcal;
        CurrentKcal = workParameter.MaxKcal;
        MaxMentalCondition = workParameter.MaxMentalCondition;
        CurrentMentalCondition = MaxMentalCondition;

        _currentAssignedTask = null;

        _currentState = DEAD_STATE;
        ChangeState(EState.IDLE);

        Scheduler.RegisterWorker(this);
    }

    public void Update()
    {
        if (reward != null)
        {
            reward.gameObject.transform.position = itemPickUpPosition.transform.position;
        }
    }

    public void ChangeState(EState newState)
    {
        if (_currentState == null)
        {
            return;
        }

        switch (newState)
        {
            case EState.DEAD:
                if (_currentState == DEAD_STATE) { return; }
                _currentState.OnExit(this);
                _currentState = DEAD_STATE;
                CurrentEnumState = EState.DEAD;
                _currentState.OnEnter(this);
                break;
            case EState.IDLE:
                if (_currentState == IDLE_STATE) { return; }
                _currentState.OnExit(this);
                _currentState = IDLE_STATE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            case EState.WALKING_TO_TASK:
                if (_currentState == WALKING_TO_TASK_STATE) { return; }
                _currentState.OnExit(this);
                _currentState = WALKING_TO_TASK_STATE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            case EState.WORKING:
                if (_currentState == WORKING_STATE) { return; }
                _currentState.OnExit(this);
                _currentState = WORKING_STATE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            case EState.WALKING_TO_BED:
                if (_currentState == WALKING_TO_BED_STATE) { return; }
                _currentState.OnExit(this);
                _currentState = WALKING_TO_BED_STATE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            case EState.SLEEPING:
                if (_currentState == SLEEPING_STATE) { return; }
                _currentState.OnExit(this);
                _currentState = SLEEPING_STATE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            case EState.WALKING_TO_FOOD_SOURCE:
                if (_currentState == WALKING_TO_FOOD_SOURCE) { return; }
                _currentState.OnExit(this);
                _currentState = WALKING_TO_FOOD_SOURCE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            case EState.EATING:
                if (_currentState == EATING_STATE) { return; }
                _currentState.OnExit(this);
                _currentState = EATING_STATE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            case EState.WALKING_TO_MENTAL_RESTORE_PLACE:
                if (_currentState == WALKING_TO_REST_STATION_STATE) { return; };
                _currentState.OnExit(this);
                _currentState = WALKING_TO_REST_STATION_STATE;
                CurrentEnumState = newState;
                _currentState.OnEnter(this);
                break;
            default:
                break;
        }
    }

    public void ResetState()
    {
        ChangeState(EState.IDLE);
        if (_currentAssignedTask != null)
        {
            _currentAssignedTask.AbortTask(this);
        }
        _currentAssignedTask = null;
        bed = null;
        reward = null;
        assignedContainer = null;
    }

    private void OnDestroy()
    {
        Scheduler.RemoveWorker(this);
    }

    public void AssignCurrentTask(IWorkTask task)
    {
        _currentAssignedTask = task;
        task.AssignWorker(this);
        ChangeState(EState.WALKING_TO_TASK);
    }

    public void RemoveCurrentTask()
    {
        _currentAssignedTask = null;
        ChangeState(EState.IDLE);
    }

    public void SearchForJob()
    {
        var allTasksGroups = new List<List<IWorkTask>>
        {
            Scheduler.GetHighestTasks(),
            Scheduler.GetHighTasks(),
            Scheduler.GetMidTasks(),
            Scheduler.GetLowTasks()
        };

        bool taskFound = false;
        var possibleTask = new List<IWorkTask>();
        for (var group = 0; group < allTasksGroups.Count; group++)
        {
            if (taskFound == true) break;
            if (allTasksGroups[group].Count != 0)
            {
                // get all task that matches worker abilities and arent finished
                foreach (var task in allTasksGroups[group])
                {
                    if (task.IsFinished() == false && task.IsWorkerSuitable(this) == true)
                    {
                        possibleTask.Add(task);
                        taskFound = true;
                    }
                }
            }
        }

        if (taskFound == false) return;

        IWorkTask curNearestTask = possibleTask[0];
        foreach (var task in possibleTask)
        {
            if (Vector3.Distance(transform.position, task.GetDestination()) <
                Vector3.Distance(transform.position, curNearestTask.GetDestination()))
            {
                curNearestTask = task;
            }
        }

        AssignCurrentTask(curNearestTask);
    }

    public void CustomUpdate(float dt)
    {
        if (CurrentKcal <= 0)
        {
            // worker is not able to work with 0 stamina
            if (_currentAssignedTask != null)
            {
                _currentAssignedTask.AbortTask(this);
                _currentAssignedTask = null;
            }

            ChangeState(EState.DEAD);

            // deadlock worker till player feeds the worker
            return;
        }

        // threshold of a worker when it is hungry
        if (CurrentKcal < (MaxKcal / 2) && _currentState != EATING_STATE)
        {
            if (Scheduler.GetContainerOfMaterialClass(MaterialClass.NUTRITION).Count(MaterialClass.NUTRITION) != 0)
            {
                if (_currentAssignedTask != null)
                {
                    _currentAssignedTask.AbortTask(this);
                    _currentAssignedTask = null;
                }
                ChangeState(EState.WALKING_TO_FOOD_SOURCE);
            }
        }

        if (_currentState == IDLE_STATE)
        {
            SearchForJob();
        }
        else if (_currentState == WORKING_STATE)
        {
            _currentAssignedTask.DoTask(this, dt);

            // if worker finishes task
            if (_currentAssignedTask != null && _currentAssignedTask.IsFinished() == true)
            {
                Scheduler.RemoveTask(_currentAssignedTask);
                _currentAssignedTask = null;
            }

            if (_currentAssignedTask == null)
            {
                ChangeState(EState.IDLE);
            }
        }

        _currentState.OnUpdate(this, dt);

        UpdateDebugText();
        UpdateWorkerUI();
    }

    private void UpdateDebugText()
    {
        debugText.text = "";
        debugText.text += "Worker: " + name + "\n";
        debugText.text += "Stamina: " + CurrentKcal + "/100\n";
        debugText.text += "Mental: " + CurrentMentalCondition + "/" + MaxMentalCondition + "\n";
        if (_currentAssignedTask != null)
        {
            debugText.text += "Assigned Task: " + _currentAssignedTask.ToString() + "\n";
        }
        else
        {
            debugText.text += "Assigned Task: none\n";
        }

        switch (CurrentEnumState)
        {
            case EState.DEAD:
                debugText.text += "<color=#880808ff>State: " + _currentState.ToString() + " </color>\n";
                break;
            case EState.IDLE:
                // red
                debugText.text += "<color=#ff0000ff>State: " + _currentState.ToString() + " </color>\n";
                break;
            // orange
            case EState.WALKING_TO_TASK:
                debugText.text += "<color=#ffa500ff>State: " + _currentState.ToString() + " </color>\n";
                break;
            // green
            case EState.WORKING:
                debugText.text += "<color=#00ff00ff>State: " + _currentState.ToString() + " </color>\n";
                break;
            // light blue
            case EState.WALKING_TO_BED:
                debugText.text += "<color=#add8e6ff>State: " + _currentState.ToString() + " </color>\n";
                break;
            // blue
            case EState.SLEEPING:
                debugText.text += "<color=#87CEEBff>State: " + _currentState.ToString() + " </color>\n";
                break;
            // pruple
            case EState.WALKING_TO_FOOD_SOURCE:
                debugText.text += "<color=#301934ff>State: " + _currentState.ToString() + " </color>\n";
                break;
            // light brown
            case EState.EATING:
                debugText.text += "<color=#AA68B6ff>State: " + _currentState.ToString() + " </color>\n";
                break;
        }

        if (_currentAssignedTask != null)
        {
            debugText.text += "Task Destination:\n" + _currentAssignedTask.GetDestination() + "\n";
        }

        if (CurrentEnumState == EState.WALKING_TO_TASK)
        {
            var watp = Agent.transform.position;
            var wcg = _currentAssignedTask.GetDestination();

            Vector2 currentPos = new(watp.x, watp.z);
            Vector2 destinationPos = new(wcg.x, wcg.z);

            // if destination is reached
            debugText.text += "TaskDistance: " + Vector3.Distance(currentPos, destinationPos) + "\n";
        }
    }

    private void UpdateWorkerUI()
    {
        workerUI.GetComponent<TextMeshProUGUI>().text = debugText.text;
    }

    public void InvokeSleep(Bed bed)
    {
        this.bed = bed;
        this.bed.AssignWorker(this);
        if (_currentState == IDLE_STATE)
        {
            ChangeState(EState.WALKING_TO_BED);
        }
    }

    public void WakeUp()
    {
        bed.RemoveWorker();
        bed = null;
        ChangeState(EState.IDLE);
    }
}
