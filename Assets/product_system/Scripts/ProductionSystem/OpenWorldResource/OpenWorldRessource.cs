using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWorldRessource : MonoBehaviour
{
    public float respawnTime = 10;
    private float currentRespawnCounter = 0;

    public TaskParameter taskParameter;
    private WorkBasedTask wa;
    public Reward reward;
    public int rewardAmount;
    public float TransportKcal = 0.35f;
    public float TransportMental = 0.35f;

    public GameObject Model;
    public Transform EntrancePoint;
    public Transform RewardExitPoint;

    private bool gathered = false;

    public void Start()
    {
        wa = new WorkBasedTask(
            taskParameter.WorkType,
            taskParameter.WorkPriority,
            taskParameter.RequiredWorkLevel,
            taskParameter.WorkAmount,
            taskParameter.CalorieConsumption,
            taskParameter.MentalConsumption,
            EntrancePoint.position);
        Scheduler.RegisterTask(wa);
        gathered = false;
    }

    private void Update()
    {
        if (wa.IsFinished() == true)
        {
            // disable the model
            gathered = true;
            wa.ResetTask();
            Scheduler.RemoveTask(wa);
            Model.SetActive(false);

            var toBeDroppedReward = Instantiate(reward);
            toBeDroppedReward.Amount = rewardAmount;
            toBeDroppedReward.transform.position = RewardExitPoint.position;
            Scheduler.RegisterTask(new TransportBasedTask(toBeDroppedReward, TransportKcal, TransportMental));
        }

        if (gathered == true) 
        {
            currentRespawnCounter += Time.deltaTime;

            if (currentRespawnCounter >= respawnTime)
            {
                currentRespawnCounter = 0;
                gathered = false;
                Scheduler.RegisterTask(wa);
                Model.SetActive(true);
            }
        }
    }


}
