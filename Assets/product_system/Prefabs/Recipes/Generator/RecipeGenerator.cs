using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class RecipeGenerator : MonoBehaviour
{
    private static string savePath = "Assets/product_system/Prefabs/Recipes/ScriptableObjects/";
    private static Item FindItem(List<Item> items, MaterialType matType)
    {
        foreach (var item in items)
        {
            if (item.MaterialType == matType) return item;
        }
        throw new System.Exception("Can not find a item with the given material type! Did you created one?");
    }

    public static List<Item> items = new();

    [MenuItem("Create/GenerateRecipes")]
    public static void GenRecipes()
    {
        var info = new DirectoryInfo(ItemGenerator.savePath);
        var fileInfo = info.GetFiles("*.asset");

        List<Item> items = new();

        foreach (var file in fileInfo)
        {
            items.Add((Item)AssetDatabase.LoadAssetAtPath(ItemGenerator.savePath + file.Name, typeof(Item)));
        }

        var recipes = new List<Recipe>();

        var arrow = ScriptableObject.CreateInstance<Recipe>().Init(
            "Arrow_recipe",
            new ItemAmountDictionary()
            {
                 { FindItem(items, MaterialType.WOOD), 1 },
                 { FindItem(items, MaterialType.STONE), 1 },
            },
            4,
            8,
            WorkType.Lumbering,
            WorkPriority.MIDDLE,
            1,
            0.35f,
            0.35f
            );
        recipes.Add(arrow);

        var fiber = ScriptableObject.CreateInstance<Recipe>().Init(
            "Fiber_recipe",
            new ItemAmountDictionary()
            {
                 { FindItem(items, MaterialType.WOOD), 2 },
            },
            1,
            8,
            WorkType.Handiwork,
            WorkPriority.MIDDLE,
            1,
            0.35f,
            0.35f
            );
        recipes.Add(fiber);

        var grapplingHook = ScriptableObject.CreateInstance<Recipe>().Init(
            "GrapplingHook_recipe",
            new ItemAmountDictionary()
            {
                 { FindItem(items, MaterialType.PALADIUM), 10 },
                 { FindItem(items, MaterialType.METAL_BAR), 10 },
                 { FindItem(items, MaterialType.FIBER), 30 },
                 { FindItem(items, MaterialType.ANCIENT_COMPONENT), 1 },
            },
            1,
            8,
            WorkType.Handiwork,
            WorkPriority.MIDDLE,
            1,
            0.35f,
            0.35f
            );
        recipes.Add(grapplingHook);

        var meatCleaver = ScriptableObject.CreateInstance<Recipe>().Init(
            "MeatCleaver_recipe",
            new ItemAmountDictionary()
            {
                 { FindItem(items, MaterialType.WOOD), 20 },
                 { FindItem(items, MaterialType.METAL_BAR), 5 },
                 { FindItem(items, MaterialType.STONE), 5 },
            },
            1,
            8,
            WorkType.Handiwork,
            WorkPriority.MIDDLE,
            1,
            0.35f,
            0.35f
            );
        recipes.Add(meatCleaver);

        var metalBar = ScriptableObject.CreateInstance<Recipe>().Init(
            "MetalBar_recipe",
            new ItemAmountDictionary()
            {
                 { FindItem(items, MaterialType.METAL_ORE), 2 },
            },
            1,
            8,
            WorkType.Handiwork,
            WorkPriority.MIDDLE,
            1,
            0.35f,
            0.35f
            );
        recipes.Add(metalBar);

        var palSphere = ScriptableObject.CreateInstance<Recipe>().Init(
            "Palsphere_recipe",
            new ItemAmountDictionary()
            {
                 { FindItem(items, MaterialType.PALADIUM), 1 },
                 { FindItem(items, MaterialType.WOOD), 3 },
                 { FindItem(items, MaterialType.STONE), 3 },
            },
            1,
            8,
            WorkType.Handiwork,
            WorkPriority.MIDDLE,
            1,
            0.35f,
            0.35f
            );
        recipes.Add(palSphere);

        foreach (var recipe in recipes)
        {
            AssetDatabase.CreateAsset(recipe, savePath + recipe.Name + ".asset");
        }
        AssetDatabase.SaveAssets();

    }
}