using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorkBasedTask : IWorkTask
{
    private Vector3 _workDestination;
    private bool _isFinished = false;
    private List<Worker> _workers = new();
    public List<Worker> Workers { get { return _workers; } }
    private WorkType _workType;
    private WorkPriority _workPriority;
    public readonly int RequiredWorkLevel;
    public readonly int WorkAmount;
    public readonly float CalorieConsumption;
    public readonly float MentalConsumption;
    private float _currentWorkAmount;

    public WorkBasedTask(
        WorkType workType,
        WorkPriority workPriority,
        int RequiredWorkLevel,
        int WorkAmount,
        float CalorieConsumption,
        float MentalConsumption,
        Vector3 workDestination)
    {
        _workType = workType;
        _workPriority = workPriority;
        this.RequiredWorkLevel = RequiredWorkLevel;
        this.WorkAmount = WorkAmount;
        _currentWorkAmount = WorkAmount;
        this.CalorieConsumption = CalorieConsumption;
        this.MentalConsumption = MentalConsumption;
        _workDestination = workDestination;
    }

    public void AssignWorker(Worker worker)
    {
        if (_workers.Contains(worker) == false)
        {
            _workers.Add(worker);
            worker.AssignCurrentTask(this);
        }
    }
    public void RemoveWorker(Worker worker)
    {
        foreach (var w in _workers)
        {
            if (w == worker)
            {
                Debug.Log("Removed worker " + worker + " from the WorkBasedTask " + this + ".");
                _workers.Remove(w);
                w.RemoveCurrentTask();
            }
        }
    }

    public void ResetTask()
    {
        _currentWorkAmount = WorkAmount;
        _isFinished = false;
        foreach (var worker in _workers)
        {
            worker.RemoveCurrentTask();
        }
    }

    public string GetCompletionInfo()
    {
        float workPowerSum = 0;
        // only count working workers and not only assigned ones
        foreach (var worker in _workers.Where(w => w.CurrentEnumState == EState.WORKING))
        {
            workPowerSum += worker.WorkPower;
        }

        if (_currentWorkAmount == 0)
        {
            return "inf";
        }
        else
        {
            return (_currentWorkAmount / (workPowerSum / 100)).ToString();
        }
    }
    public bool IsWorkerSuitable(Worker worker)
    {
        foreach (var ability in worker.WorkAbilities)
        {
            if (_workType == ability.WorkType && RequiredWorkLevel >= ability.WorkLevel)
            {
                return true;
            }
        }
        return false;
    }
    public bool IsFinished()
    {
        return _isFinished;
    }

    public string GetDebugInfo()
    {
        string info = "";
        info += "WorkBasedTask: (" + _workType + ")\n";
        info += "Assigned workers: \n";
        foreach (Worker worker in _workers)
        {
            info += worker.name + "\n";
        }
        info += "Estimated Time: " + GetCompletionInfo() + "\n";
        return info;
    }

    public Vector3 GetDestination()
    {
        return _workDestination;
    }

    public void SetDestination(Vector3 workDestination)
    {
        _workDestination = workDestination;
    }

    public WorkType GetWorkType()
    {
        return _workType;
    }

    public WorkClass GetWorkClass()
    {
        return WorkClass.Producing;
    }

    public void DoTask(Worker worker, float dt)
    {
        if (_isFinished == true) return;
        float worker_power;
        if (worker.CurrentKcal < worker.MaxKcal / 2)
        {
            worker_power = ((worker.WorkPower * 0.5f) / 100) * dt;
        }
        else
        {
            worker_power = ((worker.WorkPower * 1.0f) / 100) * dt;
        }

        var worker_level = 0;
        foreach (var ability in worker.WorkAbilities)
        {
            if (ability.WorkType == GetWorkType())
            {
                worker_level = ability.WorkLevel;
                break;
            }
        }
        worker_power *= ProductionSystemConfiguration.LevelIncrease[worker_level];
        _currentWorkAmount -= worker_power;

        // check if the taks is finished
        if (_currentWorkAmount <= 0)
        {
            _isFinished = true;
        }
    }

    public float GetCalorieConsumption()
    {
        return CalorieConsumption;
    }

    public void AbortTask(Worker worker)
    {
        _workers.Remove(worker);
        worker.RemoveCurrentTask();
        worker.ChangeState(EState.IDLE);
    }

    public float GetMentalFatigue()
    {
        return MentalConsumption;
    }

    public WorkPriority GetWorkPriority()
    {
        return _workPriority;
    }
}
