using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.U2D.Sprites;
using UnityEngine;

public enum WorkType
{
    // standart work types

    // work - producer
    Lumbering, // cutting trees
    Gathering, // harvest crops
    Mining,  // can mine rocks
    Farming, // drops wool
    Planting, // plants seeds in farms

    // work - assembly
    Handiwork, // crafting at benches
    Medicine_Production, //craft medicine

    // work - preparing + crafting
    Kindling, // heating stations + craft items
    Watering, // watering crops + power water stations
    Cooling,
    Generating_Electricity,

    // work - support
    Transporting, // on ground + harvest station...

    // work - timebased system
    Waiting,

    // recipe - craft based system for work stations
    Recipe
}

public enum WorkClass
{
    Producing,
    Assembly,
    Waiting,
    Utility,
}

public enum WorkPriority
{
    HIGHEST = 3,
    HIGH = 2,
    MIDDLE = 1,
    LOW = 0,
}

public interface IWorkTask
{
    public WorkPriority GetWorkPriority();
    public void SetDestination(Vector3 workDestination);
    public Vector3 GetDestination();
    public void DoTask(Worker worker, float dt);
    public void ResetTask();
    public void AbortTask(Worker worker);
    public string GetCompletionInfo();
    public void AssignWorker(Worker worker);
    public void RemoveWorker(Worker worker);
    public string GetDebugInfo();
    public WorkType GetWorkType();
    public WorkClass GetWorkClass();
    public bool IsWorkerSuitable(Worker worker);
    public bool IsFinished();
    public float GetCalorieConsumption();
    public float GetMentalFatigue();
}