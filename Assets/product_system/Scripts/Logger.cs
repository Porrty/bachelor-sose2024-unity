using System.Diagnostics;
using UnityEngine;

public class Logger : MonoBehaviour
{
    private static bool loggingOn = false;
    public static void Log(string logMsg)
    {
        if (loggingOn == true)
        {
            UnityEngine.Debug.Log(logMsg);
        }
    }
}