using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TaskType
{
    WorkBased,
    TimeBased,
    TransportBased
}

[Serializable]
[CreateAssetMenu(menuName = "ProductionSystem/Create/TaskParameter")]
public class TaskParameter : ScriptableObject
{
    [Header("TASK CONFIGURATION")]
    public TaskType TaskType;
    public WorkType WorkType;
    public WorkPriority WorkPriority = WorkPriority.LOW;
    public int RequiredWorkLevel = 0;
    public float CalorieConsumption = 0;
    public float MentalConsumption = 0;

    [Header("WORKBASED TASK CONFIGURATION")]
    public int WorkAmount = 0;

    [Header("WAITING TASK CONFIGURATION")]
    public float NeededTime = 0;
}
