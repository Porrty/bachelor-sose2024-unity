using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public interface IBed
{
    void AssignWorker(Worker worker);

    void RemoveWorker();

    bool IsAssigned();

    Vector3 GetPosition();
}

public class Bed : MonoBehaviour, IBed
{
    public Worker worker;

    private void Start()
    {
        Scheduler.RegisterBed(this);   
    }

    public void AssignWorker(Worker worker)
    {
        if (worker != null && this.worker == null)
        {
            this.worker = worker;
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public bool IsAssigned()
    {
        return worker != null;
    }

    public void RemoveWorker()
    {
        if (worker != null)
        {
            worker = null;
        }
    }
}
